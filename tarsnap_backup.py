import datetime
import json
import re
import subprocess
import xmlrpclib

RPC_HOST = "https://sean:redacted@irc.seanwatson.io:8282"
TARSNAP_CHANNEL = "#tarsnap-logs"

TEXTFILE_COLLECTOR_PATH = '/var/lib/node-exporter/textfile-collector/tarsnap.prom'

BACKUP_DIRS = '/home/sean/'
HOSTNAME = 'sean-desktop'
DATE_FORMAT = '%Y-%m-%d_%H-%M-%S'
TARSNAP_COMMAND = '/usr/local/bin/tarsnap -c --keyfile /root/tarsnap-write.key -f %s %s'

CONVERSIONS = {
    'B': 1,
    'kB': 1000,
    'MB': 1000000,
    'GB': 1000000000,
    'TB': 1000000000000,
}


def _send_message(text):
  s = xmlrpclib.ServerProxy(RPC_HOST, allow_none=True)
  s.send_irc_message(TARSNAP_CHANNEL, text)


def _write_stats(total_bytes, new_bytes):
  total_bytes = str(total_bytes)
  new_bytes = str(new_bytes)
  with open(TEXTFILE_COLLECTOR_PATH, 'w') as f:
    f.write('# HELP tarsnap_total_bytes The total size of backups in bytes.\n')
    f.write('# TYPE tarsnap_total_bytes gauge\n')
    f.write('tarsnap_total_bytes ' + total_bytes + '\n')
    f.write('# HELP tarsnap_new_bytes The number of bytes added by the most recent backup.\n')
    f.write('# TYPE tarsnap_new_bytes gauge\n')
    f.write('tarsnap_new_bytes ' + new_bytes + '\n')


def main():
  dt = datetime.datetime.today()
  backup_name = HOSTNAME + '-' + dt.strftime(DATE_FORMAT)
  _send_message(
      HOSTNAME + ': backing up ' + BACKUP_DIRS +
      ' to ' + backup_name)
  try:
    output = subprocess.check_output(
        TARSNAP_COMMAND % (backup_name, BACKUP_DIRS),
        stderr=subprocess.STDOUT,
        shell=True)
  except subprocess.CalledProcessError as e:
    _send_message(
        HOSTNAME + ': FAILED backup ' + backup_name + ' : ' + str(e))
  else:
    stats = 'unable to read stats'
    total_bytes = 0
    new_bytes = 0
    m = re.search(r'This archive +(\d+(\.\d+)? [^ ]+)', output)
    if m:
      stats = 'archive size ' + m.group(1)
      parts = m.group(1).split(' ')
      if len(parts) == 2 and parts[1] in CONVERSIONS.keys():
        total_bytes = int(float(parts[0]) * CONVERSIONS[parts[1]])
      m = re.search(r'New data +(\d+(\.\d+)? [^ ]+)', output)
      if m:
        stats += ', new ' + m.group(1)
        parts = m.group(1).split(' ')
        if len(parts) == 2 and parts[1] in CONVERSIONS.keys():
          new_bytes = int(float(parts[0]) * CONVERSIONS[parts[1]])
    _send_message(
        HOSTNAME + ': finished backup ' + backup_name + ' : ' + stats)
    if total_bytes > 0 and new_bytes > 0:
      _write_stats(total_bytes, new_bytes)

    
if __name__ == '__main__':
  main()
