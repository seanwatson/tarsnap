Steps for setting up
====================

Install tarsnap (you'll probably need to update the version numbers):

    $> sudo apt-get install gcc libc6-dev make libssl-dev zlib1g-dev e2fslibs-dev
    $> wget https://www.tarsnap.com/tarsnap-signing-key-2016.asc
    $> gpg2 --import tarsnap-signing-key-2016.asc
    $> wget https://www.tarsnap.com/download/tarsnap-autoconf-1.0.37.tgz
    $> wget https://www.tarsnap.com/download/tarsnap-sigs-1.0.37.asc
    $> gpg2 --decrypt tarsnap-sigs-1.0.37.asc
    $> tar -xzf tarsnap-autoconf-1.0.37.tgz
    $> cd tarsnap-autoconf-1.0.37
    $> ./configure
    $> make all
    $> sudo make install
    $> sudo cp /usr/local/etc/tarsnap.conf.sample /usr/local/etc/tarsnap.conf

Optionally edit `/usr/local/etc/tarsnap.conf` to enable humanized output.

Set nodump flag on things you don't want to back up:

    $> chattr +d <dir/file>

Clone the repo:

    $> git clone https://gitlab.com/seanwatson/tarsnap.git
    $> cd tarsnap

Edit the configuration values at the top of `tarsnap_backup.py`.

Generate a passphrased keyfile. It will ask for your password:

    $> sudo tarsnap-keygen --keyfile /root/tarsnap.key \
        --user user@email.com \
        --machine my-machine \
        --passphrased

Generate a passwordless, write-only keyfile:

    $> sudo tarsnap-keymgmt --outkeyfile /root/tarsnap-write.key \
        -w /root/tarsnap.key

Make sure things work by adding `--dry-run` to the command and running
`sudo python /root/tarsnap_backup.py`.

Once it works add a cron job like below with `sudo crontab -e`.

    0 3 * * * python /root/tarsnap_backup.py

Copy the keys to somewhere safe.
